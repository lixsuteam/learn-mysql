package com.lixionary.testmysql;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class.getName());

    public static void main(String[] args) {
        setup();

        // change parameters below, use TH db for these shipper ids in query!!
        final String DB_URI = "x";
        final String DB_USER = "y";
        final String DB_PASS = "z";

        //fetch size > 0: standard fetch
        //fetch size Integer.MIN_VALUE: mysql stream
        final int FETCH_SIZE = 1000;

        final List<Long> resultSetAcquiredTimeList = new ArrayList<>();
        final List<Long> resultSetIterationTimeList = new ArrayList<>();
        final List<Long> fullExecutionTimeList = new ArrayList<>();

        LOGGER.info("fetch size: {}", FETCH_SIZE);
        for (int i = 0; i < 10; i++) {
            final List<Long> orderIds = new ArrayList<>();
            Connection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;
            long startTime = Instant.now().toEpochMilli();
            long t1 = 0, t2 = 0, t3 = 0;
            try {
                LOGGER.info("starting db connection...");
                connection = DbUtil.createConnection(DB_URI, DB_USER, DB_PASS);
                statement = DbUtil.getStatement(connection, FETCH_SIZE);

                LOGGER.info("executing queries");
                t1 = Instant.now().toEpochMilli();
                resultSet = statement.executeQuery(getQuery());
                t2 = Instant.now().toEpochMilli();
                resultSetAcquiredTimeList.add(t2 - t1);
                while (resultSet.next()) {
                    Long orderId = resultSet.getLong("id");
                    orderIds.add(orderId);
                }
                t3 = Instant.now().toEpochMilli();

                resultSetIterationTimeList.add(t3 - t2);
            } catch (SQLException ex) {
                LOGGER.error(ex.getMessage() + ex.getSQLState());
            } finally {
                DbUtil.tryCloseResultSet(resultSet);
                DbUtil.tryCloseStatement(statement);
                DbUtil.tryCloseConnection(connection);
            }

            long endTime = Instant.now().toEpochMilli();
            LOGGER.info("LOOP - {} finished==============", i);
            LOGGER.info("result set acquired in {} ms", t2 - t1);
            LOGGER.info("result set iterated in {} ms", t3 - t2);
            LOGGER.info("procedure finished in {} ms", endTime - startTime);
            fullExecutionTimeList.add(endTime - startTime);
            LOGGER.info("finished read all orders [{}]", orderIds.size());
            LOGGER.info("LOOP - {} end===================", i);
        }

        System.out.println(resultSetAcquiredTimeList);
        System.out.println(resultSetIterationTimeList);
        System.out.println(fullExecutionTimeList);

    }

    private static void setup() {
        BasicConfigurator.configure();
        final String dbDriver = "com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(dbDriver);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private static String getQuery() {
        return "SELECT o.`id`, o.`status`, o.`granular_status`, o.`created_at`, o.`updated_at`\n" +
                "  FROM `orders` AS o FORCE INDEX (`shipper_id_granular_status`)\n" +
                "WHERE `shipper_id` IN (42617,39005,39788,43022,40132,38583,74229,37352,38381,39035,38386,45478,37282,38911,39105,31505,61908,36671,36745,36843,37053,32211,36647,39215,62391,42654,56349,50036,66931,63770,71205,56782,51743,36836,68123,47177,52771,37716,37808,37988,52217,39085,48331,36660,38188,62355,63016,52328,72363,37267,39248,37176,34012,36896,38030,43597,37429,40723,45835,38452,39064,36622,43804,50799,56465,38584,70117,56875,53190,54669,51656,36858,42961,57375,45816,36825,53871,65187,51783,41856,54239,47216,55170,48544,52862,66847,57817,52168,36704,37209,37425,37436,40056,60844,54503,59034,61508,67885,74012,37073,38877,37629,39575,43571,51374,52127,51053,37331,39177,59201,61631,55657,54886,51868,68926,52505,72459,63096,55985,54273,36680,43159,46093,62074,36672,39136,37466,37893,63745,64790,69544,64033,54955,47688,46077,62283,40087,38352,59761,69949,51468,44268,43115,55972,36618,37173,68840,44676,58814,55538,56943,61408,46964,38127,34073,39446,38420,37091,44262,45262,33157,36898,37444,37448,31506,33975,34019,39517,37432,39020,37000,38133,34043,38404,38621,38255,47420,42453,37051,39014,56229,36847,37038,37592,37433,45388,65307,52781,41305,66839,42869,36633,44709,69319,60013,72493,49554,33971,44472,60068,67331,54731,44888,40073,49628,72557,60840,32748,55517,36625,36903,52782,40567,36699,36908,38912,61164,43440,42676,51556,62359,73171,73772,74585,37451,38239,43162,36664,47469,38913,49360,47527,39659,37322,50795,62374,55513,38872,33633,40701,69773,63417,45188,36679,46525,61231,67368,67346,50225,56989,68693,66743,46769,52413,47738,57528,36885,37078,37977,38370,64394,63005,39207,39812,63044,68688,72514,67128,70245,50046,60827,37965,53227,65016,69390,68622,40915,72377,55492,70794,70665,70990,45582,45610,48897,54339,37069,37321,32207,38403,36696,39210,37807,73285,74589,74831\n" +
                ")\n" +
                "  AND `created_at` BETWEEN '2019-03-20 16:00:00' AND '2019-05-10 15:59:59'\n" +
                "  AND `granular_status` IN ('Returned to sender', 'Completed')\n" +
                "  AND `status` = 'Completed'\n" +
                "  AND `deleted_at` IS NULL\n" +
                "ORDER BY o.`id` ASC;";
    }
}
