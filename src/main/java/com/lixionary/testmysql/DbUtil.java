package com.lixionary.testmysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

class DbUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbUtil.class.getName());

    @SuppressWarnings("SameParameterValue")
    static Connection createConnection(String dbUri, String dbUser, String dbPassword) throws SQLException {
        Connection connection = DriverManager.getConnection(dbUri, dbUser, dbPassword);
        connection.setAutoCommit(false);
        return connection;
    }

    @SuppressWarnings("SameParameterValue")
    static Statement getStatement(Connection connection, int fetchSize) throws SQLException {
        Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        statement.setFetchSize(fetchSize);
        return statement;
    }

    static void tryCloseConnection(Connection connection) {
        LOGGER.info("trying to close connection...");
        if (connection != null) {
            try {
                if (!connection.isClosed()) {
                    connection.close();
                    LOGGER.info("connection closed!");
                }
            } catch (Exception ex) {
                LOGGER.error("failed to close opened connection!", ex);
            }
        }
    }

    static void tryCloseStatement(Statement statement) {
        LOGGER.info("trying to close statement...");
        if (statement != null) {
            try {
                if (!statement.isClosed()) {
                    statement.close();
                    LOGGER.info("statement closed!");
                }
            } catch (Exception ex) {
                LOGGER.error("failed to close opened statement!", ex);
            }
        }
    }

    static void tryCloseResultSet(ResultSet resultSet) {
        LOGGER.info("trying to close result set...");
        if (resultSet != null) {
            try {
                if (!resultSet.isClosed()) {
                    resultSet.close();
                    LOGGER.info("result set closed!");
                }
            } catch (Exception ex) {
                LOGGER.error("failed to close opened result set!", ex);
            }
        }
    }
}
