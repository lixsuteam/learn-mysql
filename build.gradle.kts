plugins {
    java
    application
}

allprojects {
    group = "com.lixionary"
    version = "1.0.0"
}

repositories {
    mavenCentral()
}

application {
    mainClassName = "com.lixionary.testmysql.App"
}

dependencies {
    implementation("org.slf4j:slf4j-log4j12:1.7.26")
    testImplementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("org.bouncycastle:bcprov-jdk16:1.46")
    implementation("org.apache.commons:commons-lang3:3.0")
    implementation("org.apache.commons:commons-text:1.6")
    implementation("mysql:mysql-connector-java:8.0.13")

    implementation("commons-codec:commons-codec:1.11")
    implementation("com.fasterxml.uuid:java-uuid-generator:3.1.5")
    implementation("com.googlecode.libphonenumber:libphonenumber:8.9.9")
    implementation("io.reactivex.rxjava2:rxjava:2.2.4")
    implementation("com.squareup.okhttp3:okhttp:3.14.1")
    implementation("com.sendgrid:sendgrid-java:4.3.0")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.9.8")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.9.8")
    implementation("com.fasterxml.jackson.module:jackson-module-parameter-names:2.9.8")

    testImplementation("junit:junit:4.12")

}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}